<?php

namespace App\Providers;

use App\Services\Telegram\TelegramBotService;
use App\Services\Telegram\TelegramWebhookHandlerResolverService;
use App\Services\TokenLoginService;
use Filament\Facades\Filament;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            TelegramBotService::class,
            fn (Application $app) => new TelegramBotService(
                $app['config']->get('services.telegram-bot-api.base_uri'),
                $app['config']->get('services.telegram-bot-api.token'),
            )
        );

        $this->app->bind(
            TelegramWebhookHandlerResolverService::class,
            fn (Application $app) => new TelegramWebhookHandlerResolverService(
                $app['config']->get('office_light.telegram.webhook.handler.namespace'),
                $app['config']->get('office_light.telegram.webhook.handler.path')
            )
        );

        $this->app->bind(
            TokenLoginService::class,
            fn (Application $app) => new TokenLoginService(
                $app['config']->get('office_light.login_token_expiration_time_in_minutes'),
            )
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        Filament::registerRenderHook(
            'footer.end',
            fn (): View => view('filament.footer.end'),
        );
    }
}
