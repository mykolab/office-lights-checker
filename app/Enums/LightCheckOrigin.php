<?php

namespace App\Enums;

enum LightCheckOrigin: string
{
    case CRON = 'cron';

    case USER = 'user';

    public static function values(): array
    {
        return array_map(
            fn(self $item) => $item->value,
            self::cases()
        );
    }
}
