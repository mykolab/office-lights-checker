<?php

namespace App\EloquentBuilders;

use Illuminate\Database\Eloquent\Builder;

class LoginTokenBuilder extends Builder
{
    public function expired(): static
    {
        return $this->where('expires_at', '<', now());
    }

    public function notExpired(): static
    {
        return $this->where('expires_at', '>=', now());
    }
}
