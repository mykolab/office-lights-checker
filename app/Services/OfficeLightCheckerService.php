<?php

namespace App\Services;

use App\Enums\LightCheckOrigin;
use App\Events\LightCheckPerformedEvent;
use App\Models\LightCheck;
use Symfony\Component\Process\Process;

class OfficeLightCheckerService
{
    public function runCheck(): void
    {
        $lightCheck = $this->performLightCheck();

        event(new LightCheckPerformedEvent($lightCheck));
    }

    /**
     * Note that ike-scan tool require root access.
     *
     * @return LightCheck
     */
    public function performLightCheck(LightCheckOrigin $origin = LightCheckOrigin::CRON): LightCheck
    {
        $ipAddress = config('office_light.ip_address');

        $ikeScanProcess = new Process(['ike-scan', '-M', $ipAddress,]);
        $ikeScanProcess->run();

        if (! $ikeScanProcess->isSuccessful()) {
            return LightCheck::query()->create([
                'ip_address' => $ipAddress,
                'light_available' => false,
                'output_description' => $ikeScanProcess->getErrorOutput(),
                'origin' => $origin,
            ]);
        }

        $output = $ikeScanProcess->getOutput();
        preg_match('/\)\.(.*)(\d*) returned handshake/', $output, $matches);

        $handshakeSuccessful = (bool) trim($matches[1] ?? '');

        return LightCheck::query()->create([
            'ip_address' => $ipAddress,
            'light_available' => $handshakeSuccessful,
            'output_description' => $output,
            'origin' => $origin,
        ]);
    }
}
