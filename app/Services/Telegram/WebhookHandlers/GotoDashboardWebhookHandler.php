<?php

namespace App\Services\Telegram\WebhookHandlers;

use App\Attributes\TelegramWebhookAction;
use App\DTO\TelegramInlineKeyboardButton;
use App\DTO\TelegramWebhookData;
use App\Jobs\SendMessageToTelegramChatJob;
use App\Models\User;
use App\DTO\TelegramMessage;
use App\Services\TokenLoginService;

#[TelegramWebhookAction('/goto_dashboard')]
readonly class GotoDashboardWebhookHandler implements WebhookHandler
{
    public function __construct(
        private TokenLoginService $tokenLoginService,
    ) {
    }

    public function handle(TelegramWebhookData $telegramWebhookData): void
    {
        /** @var User $user */
        $user = User::query()->where('email', config('office_light.admin.default_email'))->first();

        if (! $user) {
            return;
        }

        $telegramMessage = TelegramMessage::make(__('office_light.goto_dashboard.text'), $telegramWebhookData->chatId)
            ->urlButton(
                TelegramInlineKeyboardButton::make(
                    __('office_light.goto_dashboard.button'),
                    $this->tokenLoginService->generateLoginLink($user)
                )
            );

        SendMessageToTelegramChatJob::dispatch($telegramMessage);
    }
}
