<?php

namespace App\Services\Telegram\WebhookHandlers;

use App\DTO\TelegramWebhookData;

interface WebhookHandler
{
    public function handle(TelegramWebhookData $telegramWebhookData): void;
}
