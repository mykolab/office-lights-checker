<?php

namespace App\Services\Telegram;

use App\Attributes\TelegramWebhookAction;
use App\DTO\TelegramWebhookData;
use App\Services\Telegram\WebhookHandlers\WebhookHandler;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use ReflectionAttribute;
use ReflectionClass;
use SplFileInfo;

readonly class TelegramWebhookHandlerResolverService
{
    public function __construct(
        private string $webhookHandlerNamespace,
        private string $webhookHandlerPath
    ) {
    }

    public function resolveWebhookHandler(TelegramWebhookData $telegramWebhookData): WebhookHandler|null
    {
        $webhookHandlerClass = $this->resolveWebhookHandlerClass($telegramWebhookData);

        if (is_null($webhookHandlerClass)) {
            return null;
        }

        return app()->make($webhookHandlerClass);
    }

    private function resolveWebhookHandlerClass(TelegramWebhookData $telegramWebhookData): string|null
    {
        return collect(
            File::files($this->webhookHandlerPath)
        )
            ->map(function (SplFileInfo $fileInfo) {
                return (string) Str::of($fileInfo->getRealPath())
                    ->afterLast($this->webhookHandlerPath)
                    ->replace('/', '\\')
                    ->beforeLast('.php')
                    ->prepend($this->webhookHandlerNamespace);
            })
            ->filter(
                fn(string $className) => class_exists($className)
            )
            ->first(function (string $className) use ($telegramWebhookData) {
                /** @var ?TelegramWebhookAction $telegramWebhookAction */
                $telegramWebhookAction = collect(
                    (new ReflectionClass($className))->getAttributes()
                )->map(
                    fn(ReflectionAttribute $attribute) => $attribute->newInstance()
                )->first(function ($attribute) {
                    return $attribute instanceof TelegramWebhookAction;
                });

                return $telegramWebhookAction?->isValidAction($telegramWebhookData->messageText);
            });
    }
}