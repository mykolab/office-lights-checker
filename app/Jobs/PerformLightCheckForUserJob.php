<?php

namespace App\Jobs;

use App\DTO\LightCheckStatusData;
use App\Enums\LightCheckOrigin;
use App\Services\OfficeLightCheckerService;
use App\DTO\TelegramMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PerformLightCheckForUserJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(private readonly string $telegramChatId)
    {
    }

    public function handle(OfficeLightCheckerService $officeLightCheckerService)
    {
        $lightCheck = $officeLightCheckerService->performLightCheck(LightCheckOrigin::USER);

        $lightCheckStatusData = LightCheckStatusData::from($lightCheck);

        SendMessageToTelegramChatJob::dispatch(
            TelegramMessage::make($lightCheckStatusData->getStatusMessage(), $this->telegramChatId)
        );
    }
}