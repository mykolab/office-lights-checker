<?php

namespace App\DTO;

use Illuminate\Support\Arr;

readonly class TelegramInlineKeyboardButton
{
    public function __construct(public string $label, public string $url) {

    }

    public static function make(string $label, string $url): static
    {
        return new static($label, $url);
    }

    public function toArray(): array
    {
        return [
            'text' => $this->label,
            'url' => $this->url,
        ];
    }
}
