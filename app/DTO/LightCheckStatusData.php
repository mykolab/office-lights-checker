<?php

namespace App\DTO;

use App\Models\LightCheck;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

readonly class LightCheckStatusData
{
    public function __construct(
        public bool $lightAvailable,
        public Carbon $lastCheckAt,
        public Carbon $since,
    ) {
    }

    public function getStatusMessage(): string
    {
        return view('telegram_messages.light_status_message', [
            'lightCheckStatusData' => $this,
        ]);
    }

    public static function from(LightCheck $lightCheck): static
    {
        /** @var LightCheck|null $lastOpositeStatusLightCheck */
        $lastOpositeStatusLightCheck = LightCheck::query()
            ->origin()
            ->where('light_available', ! $lightCheck->light_available)
            ->latest()
            ->first();

        /** @var LightCheck|null $lastSameStatusLightCheck */
        $lastSameStatusLightCheck = LightCheck::query()
            ->where('id', '>', $lastOpositeStatusLightCheck?->id)
            ->orderBy('id')
            ->first();

        $since = $lastSameStatusLightCheck->created_at ?? $lightCheck->created_at;

        return new static($lightCheck->light_available, $lightCheck->created_at, $since);
    }

    public static function empty(): static
    {
        return new static(true, Carbon::now(), Carbon::now());
    }
}