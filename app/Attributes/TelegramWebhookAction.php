<?php

namespace App\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
readonly class TelegramWebhookAction
{
    /**
     * @var string[]
     */
    public array $actions;

    /**
     * @param string[]|string $action
     */
    public function __construct(array|string $action)
    {
        $this->actions = (array) $action;
    }

    public function isValidAction(string $action): bool
    {
        return collect($this->actions)
            ->filter(
                fn(string $item) => str($action)->startsWith($item)
            )->isNotEmpty();
    }
}