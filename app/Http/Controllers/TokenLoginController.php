<?php

namespace App\Http\Controllers;

use App\Services\TokenLoginService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TokenLoginController extends Controller
{
    public function __construct(
        private readonly TokenLoginService $tokenLoginService
    ) {
    }

    public function __invoke(Request $request, string $token): RedirectResponse
    {
        if (str($request->userAgent())->contains('TwitterBot')) {
            abort(404);
        }

        if (auth()->check()) {
            return redirect()->route('filament.pages.dashboard');
        }

        $loginToken = $this->tokenLoginService->resolveLoginToken($token);

        if (! $loginToken) {
            return redirect()->route('filament.auth.login');
        }

        Auth::login($loginToken->user);
        $loginToken->delete();

        return redirect()->route('filament.pages.dashboard');
    }
}