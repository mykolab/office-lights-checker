<?php

namespace App\Http\Controllers;

use App\Services\Telegram\TelegramWebhookService;
use Illuminate\Http\Request;

class TelegramWebhookController extends Controller
{
    public function __construct(private readonly TelegramWebhookService $telegramWebhookService)
    {
    }

    public function index(Request $request)
    {
        $this->telegramWebhookService->handleWebhook($request->all());

        return response()->json([
            'message' => 'ok',
        ]);
    }
}