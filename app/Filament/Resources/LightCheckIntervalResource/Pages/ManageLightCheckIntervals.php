<?php

namespace App\Filament\Resources\LightCheckIntervalResource\Pages;

use App\Filament\Resources\LightCheckIntervalResource;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckDataRetentionInfo;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckIntervalsInfo;
use Filament\Resources\Pages\ManageRecords;

class ManageLightCheckIntervals extends ManageRecords
{
    protected static string $resource = LightCheckIntervalResource::class;

    protected function getActions(): array
    {
        return [
            //
        ];
    }

    protected function getHeaderWidgets(): array
    {
        return [
            LightCheckDataRetentionInfo::class,
            LightCheckIntervalsInfo::class,
        ];
    }
}
