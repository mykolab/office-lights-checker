<?php

namespace App\Filament\Resources\LightCheckResource\Widgets;

use Filament\Widgets\Widget;

class LightCheckDataRetentionInfo extends Widget
{
    protected static string $view = 'filament.resources.light-check-resource.widgets.light-check-data-retention-info';

    public int $days = 1;

    public function mount(): void
    {
        $this->days = config('office_light.light_checks_expiration_time_in_days');
    }
}
