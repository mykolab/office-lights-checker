<?php

namespace App\Filament\Resources\LightCheckResource\Widgets;

use Filament\Widgets\Widget;

class LightCheckIntervalsInfo extends Widget
{
    protected static string $view = 'filament.resources.light-check-resource.widgets.light-check-intervals-info';

    public int $minutes = 1;

    public function mount(): void
    {
        $this->minutes = config('office_light.light_checks_interval_in_minutes');
    }
}
