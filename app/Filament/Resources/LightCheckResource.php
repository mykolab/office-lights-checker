<?php

namespace App\Filament\Resources;

use App\EloquentBuilders\LightCheckBuilder;
use App\Enums\LightCheckOrigin;
use App\Filament\Resources\LightCheckResource\Pages;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckDataRetentionInfo;
use App\Filament\Resources\LightCheckResource\Widgets\LightCheckIntervalsInfo;
use App\Models\LightCheck;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Forms;
use Illuminate\Database\Eloquent\Builder;

class LightCheckResource extends Resource
{
    protected static ?string $model = LightCheck::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('id'),
                Forms\Components\Toggle::make('light_available'),
                Forms\Components\Textarea::make('output_description'),

            ])->columns(1);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable(),
                Tables\Columns\TextColumn::make('ip_address'),
                Tables\Columns\IconColumn::make('light_available')->boolean(),
                Tables\Columns\TextColumn::make('created_at')->dateTime()->sortable(),
            ])
            ->filters([
                Tables\Filters\TernaryFilter::make('light_available'),
                Tables\Filters\Filter::make('created_at')
                    ->form([
                        Forms\Components\DateTimePicker::make('created_from'),
                        Forms\Components\DateTimePicker::make('created_until'),
                    ])
                    ->query(function (Builder $query, array $data): Builder {
                        return $query
                            ->when(
                                $data['created_from'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '>=', $date),
                            )
                            ->when(
                                $data['created_until'],
                                fn (Builder $query, $date): Builder => $query->where('created_at', '<=', $date),
                            );
                    })
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
            ])
            ->defaultSort('id', 'desc');
    }

    public static function getWidgets(): array
    {
        return [
            LightCheckDataRetentionInfo::class,
            LightCheckIntervalsInfo::class,
        ];
    }

    public static function getEloquentQuery(): Builder|LightCheckBuilder
    {
        return parent::getEloquentQuery()->origin();
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageLightChecks::route('/'),
        ];
    }    
}
