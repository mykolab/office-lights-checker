<?php

namespace App\Filament\AvatarProviders;

use Filament\AvatarProviders\Contracts\AvatarProvider;
use Illuminate\Database\Eloquent\Model;

class DefaultAvatarProvider implements AvatarProvider
{
    public function get(Model $user): string
    {
        return asset('images/tallium-logo.svg');
    }
}
