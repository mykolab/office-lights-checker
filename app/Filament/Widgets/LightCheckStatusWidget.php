<?php

namespace App\Filament\Widgets;

use App\DTO\LightCheckStatusData;
use App\Models\LightCheck;
use Filament\Widgets\Widget;

class LightCheckStatusWidget extends Widget
{
    protected static ?int $sort = -1;

    protected static string $view = 'filament.widgets.light-check-status-widget';

    private const DATE_FORMAT = 'M d, Y H:i:s';

    public bool $lightAvailable = true;

    public string $fromDate;

    public string $lastCheckDate;

    public function mount()
    {
        /** @var LightCheck|null $lastLightCheck */
        $lastLightCheck = LightCheck::query()->origin()->latest()->first();

        $lightCheckStatusData = $lastLightCheck
            ? LightCheckStatusData::from($lastLightCheck)
            : LightCheckStatusData::empty();

        $this->lightAvailable = $lightCheckStatusData->lightAvailable;
        $this->fromDate = $lightCheckStatusData->since->format(self::DATE_FORMAT);
        $this->lastCheckDate = $lightCheckStatusData->lastCheckAt->format(self::DATE_FORMAT);
    }
}
