<?php

namespace App\Events;

use App\Models\LightCheck;
use Illuminate\Foundation\Events\Dispatchable;

class LightCheckPerformedEvent
{
    use Dispatchable;

    public function __construct(public readonly LightCheck $lightCheck)
    {
    }
}