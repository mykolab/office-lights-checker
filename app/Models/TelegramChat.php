<?php

namespace App\Models;

use App\DTO\TelegramWebhookData;
use App\Models\Scopes\TestModeScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $telegram_chat_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon|null $deleted_at
 */
class TelegramChat extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'telegram_chat_id',
    ];

    protected static function booted()
    {
        if (config('office_light.test_mode.enabled') === true) {
            static::addGlobalScope(new TestModeScope());
        }
    }

    public static function updateOrCreateFromWebhookData(TelegramWebhookData $telegramWebhookData): static
    {
        return TelegramChat::query()->updateOrCreate(
            [
                'telegram_chat_id' => $telegramWebhookData->chatId,
            ],
            [
                'name' => $telegramWebhookData->username,
                'telegram_chat_id' => $telegramWebhookData->chatId,
            ]
        );
    }
}
