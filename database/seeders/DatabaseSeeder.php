<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $defaultEmail = config('office_light.admin.default_email');

        if (User::query()->where('email', $defaultEmail)->exists()) {
            return;
        }

        User::factory()->create([
            'name' => 'Tallium',
            'email' => $defaultEmail,
            'password' => Hash::make(config('office_light.admin.default_password')),
        ]);
    }
}
