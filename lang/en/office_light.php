<?php

return [
    'goto_dashboard' => [
        'text' => '❕ Click the button to access the web dashboard',
        'button' => 'Dashboard',
    ],
    'telegram_bot' => 'Telegram bot',

    'data_retention_note' => '{1} This app keeps data only for last :value day.|[2,*] This app keeps data only for last :value days.',
    'light_check_intervals_note' => '{1} The light check is performed automatically every :value minutes.|[2,*] The light check is performed automatically every :value minutes.',
];
