<x-filament::widget>
    <x-filament::card>
        <div class="flex space-x-2">
            <x-heroicon-o-information-circle class="w-6 h-6" />
            <div class="my-auto">
                {{ trans_choice('office_light.light_check_intervals_note', $minutes, ['value' => $minutes]) }}
            </div>
        </div>
    </x-filament::card>
</x-filament::widget>
