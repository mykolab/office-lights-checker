@if($lightCheckStatusData->lightAvailable)
    *✅ The light* _(internet connection)_ *in the office is ON*
@else
    *❌ The light* _(internet connection)_ *in the office is OFF*
@endif

_Since:_ *{{ $lightCheckStatusData->since->format('d.m.Y H:i:s') }}* _(Kyiv timezone)_
_The last check was made at:_ *{{ $lightCheckStatusData->lastCheckAt->format('d.m.Y H:i:s') }}* _(Kyiv timezone)_
